# DiCoExpress

![R](https://img.shields.io/badge/R-≥3.5.0-blue.svg) ![RNAseq](https://img.shields.io/badge/analysis-RNAseq-green.svg)

## Contents

- [Installation](#installation)
- [Organization of the Git Repository](#organization-of-the-git-repository)
- [Mailing list](#mailing-list)
- [Citation](#citation)

## What is DiCoExpress?

DiCoExpress is a workspace developed in R for scientists, not experts in statistics, and with knowledge of R, for the analysis of RNA-seq datasets.
DiCoExpress performs quality controls, proposes generalized linear model (GLM) analyses, with automated contrasts writing, allowing the user to answer to several biological questions.
Finally, DiCoExpress conducts a coexpression analysis using mixture models.
Statistical results of the differential analysis, as well as the coexpression analysis, can be completed with gene annotations.
Annotations enrichment against a reference set can be tested.

## What is the differences between the versions?

The first version was proposed in 2020 and the functions of this version are now in Sources_v1.
A second version exists now and the functions  are now in Sources_v2.

The differences between both are described in the document
`Version1_vs_Version2.pdf`  available at the root of the directory DiCoExpress

## How to install DiCoExpress? 

After downloading DiCoExpress either as a zip file or using the command "git clone" in Shell.
1. Open R or Rstudio (if installed) with R version >=3.5.0 The working directory must be DiCoExpress/Template_scripts
2. Install all CRAN R packages used in DiCoExpress by running the R installation program : 

``source_path = "Sources_v2/"``

``source(paste0("../",source_path,"Install_Packages.R"))``

source_path is the name of the folder containing the Source code. 
You can choose to use version 1 by setting source_path = "Sources_v1".

If all packages are successfully installed, 
the logical value “TRUE” will be printed on the R Console pane for each package. 
If the logical value “FALSE” is printed, you will need to check the error messages 
in the Console pane to find a solution.

The `DiCoExpress_TP.html` file is a tutorial explaining step by step an analysis. 

## Organization of the Git Repository

The Git repository is composed of DiCoExpress and pdf files.

* DiCoExpress consists of several directories :

    - **Data/**: contains the input data required to run an analysis DiCoExpress

    - **Sources_v1/** and **Sources_v2/**: host the source code of DiCoExpress. Version 2 offers modifications and improvements         compared to version 1, the differences are explained in the `Version1_vs_Version2.pdf` file.


    - **Template_scripts/**: contains an R script for each project, allowing a semi-automated data analysis where the user is guided. In this directories, `DiCoExpress_TP.R` was designed to facilitate the handling of the tool version v2.
The script `DiCoExpress_Tutorial_JoVE.R` was designed for the version v1 of DiCoExpress. We decided to remove `DiCoExpress_Brassica_napus.R`, which is not more relevant. 
    
    - **Results/**: contains a subdirectory per project with all the results of the different steps. 

* The `DiCoExpress_Reference_Manual.pdf` file is a full description of the seven functions of DiCoExpress v2.
This document provides a detailled description of all the arguments of the functions.

* The `DiCoExpress_TP.html` file is a tutorial explaining step by step the script `DiCoExpress_TP.R`.  We created this tutorial to help you to understand how to adapt the script for your own dataset.


## How to cite DiCoExpress?

* Lambert, I., Paysant-Le Roux, C., Colella, S., Martin-Magniette, M.-L. DiCoExpress: a tool to process multifactorial RNAseq experiments from quality controls to co-expression analysis through differential analysis based on contrasts inside GLM models. Plant Methods 16, 68 (2020). https://doi.org/10.1186/s13007-020-00611-7
* Baudry, K., Paysant-Le Roux, C., Colella, S., Castandet, B., Martin, M.L. (2021) Analyzing Multifactorial RNA-Seq Experiments with DicoExpress. J. Vis. Exp. (), e62566, doi:10.3791/62566. 


## Mailing list

Please subscribe to the mailing list: https://groupes.renater.fr/sympa/subscribe/dicoexpress to follow the modifications done on DiCoExpress and to ask questions
