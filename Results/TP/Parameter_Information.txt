Mon Feb 19 14:21:17 2024 
Project_Name: TP 
Filter: NULL 
Filter_Strategy: filterByExpr of edger 
Min.count:  15 
Normalization_Method:  TMM 
Replicate:  TRUE 
Interaction:  TRUE 
NbGenes_Profiles:  20 
NbGenes_Clustering:  50 
Alpha_DiffAnalysis:  0.05 
Alpha_Enrichment:  0.4 
Groups (Contrats_Comparison):  [Genotype1_control-Genotype1_treated]-[Genotype2_control-Genotype2_treated] [Genotype1_control-Genotype1_treated]-[Genotype3_control-Genotype3_treated] [Genotype1_control-Genotype1_treated]-[Genotype4_control-Genotype4_treated] [Genotype2_control-Genotype2_treated]-[Genotype3_control-Genotype3_treated] [Genotype2_control-Genotype2_treated]-[Genotype4_control-Genotype4_treated] 
Groups (Coexpression):  [Genotype1_control-Genotype1_treated]-[Genotype2_control-Genotype2_treated] [Genotype1_control-Genotype1_treated]-[Genotype3_control-Genotype3_treated] [Genotype1_control-Genotype1_treated]-[Genotype4_control-Genotype4_treated] [Genotype2_control-Genotype2_treated]-[Genotype3_control-Genotype3_treated] [Genotype2_control-Genotype2_treated]-[Genotype4_control-Genotype4_treated] 
